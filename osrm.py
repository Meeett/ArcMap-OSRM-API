#   Project:    OSRM-Routing-API-ArcMap
#   Author:     Mike Wunderlich
#   Mail:       GIS@mikewunderlich.de
#   Version:    1.0 - Working

import arcpy
import json
import urllib2
import sys
import os

#Inputs
inFC = arcpy.GetParameterAsText(0)
inServer = arcpy.GetParameterAsText(1)
inProxyUser = arcpy.GetParameterAsText(2)
inProxyPass = arcpy.GetParameterAsText(3)
outFC = arcpy.GetParameterAsText(4)

#generateRequest
points = ''
indexFC = 0
for row in arcpy.da.SearchCursor(inFC, ["SHAPE@XY"]):
    # Print x,y coordinates of each point feature
    #
    x, y = row[0]
    if indexFC != 0:
        points = points + ";"
    points = points + str(x)+","+str(y)
    indexFC = indexFC + 1

#Check Number of Points
if not indexFC >1:
    arcpy.AddError("Es wurden zu wenig Punkte angegeben!")
    sys.exit(0)

#URL
server = 'http://router.project-osrm.org/'
serverMike = 'http://osrm.mikewunderlich.de/'
url = inServer+'route/v1/driving/'+points+'?alternatives=true&geometries=geojson&overview=full'
arcpy.AddMessage(url)


#Proxy
proxy_handler = urllib2.ProxyHandler({'http': inProxyUser+':'+inProxyPass+'@prz.proxy.zdi.svc:80'})
opener = urllib2.build_opener(proxy_handler)
urllib2.install_opener(opener)
data = json.load(urllib2.urlopen(url))

#Vorlage
FCvorlage = os.getcwd()+r'\tmp.gdb\vorlage'
FCresult = outFC
arcpy.CopyFeatures_management(FCvorlage, FCresult)

#CalcTime
def calcTime(getDuration):
    secounds = int((getDuration) % 60)
    minutes = int((getDuration / (60)) % 60)
    hours = int((getDuration / (60*60)) % 24)
    
    if len(str(hours)) != 2:
        hours = "0"+str(hours)
    if len(str(minutes)) != 2:
        minutes = "0"+str(minutes)
    if len(str(secounds)) != 2:
        secounds = "0"+str(secounds)

    return str(hours)+":"+str(minutes)+":"+str(secounds)

#WriteLine
def writeLine(getPolyline, getDistance, getDuration, getDurationTime):
    print "writeLine"
    cursor = arcpy.da.InsertCursor(FCresult, ['SHAPE@', 'distance', 'duration', 'durationTime',])
    cursor.insertRow([getPolyline, getDistance, getDuration, getDurationTime])
    del cursor

#Calc
index = 1
for route in data['routes']:
    print "-----"
    print route['distance']
    print route['duration']
    
    array = arcpy.Array()
    for point in route['geometry']['coordinates']:
        array.add(arcpy.Point(point[0],point[1]))
    print "-----"
    polyline = arcpy.Polyline(array)
    writeLine(polyline,route['distance'],route['duration'],calcTime(route['duration']))
